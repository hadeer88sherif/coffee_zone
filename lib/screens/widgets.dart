  
import 'package:coffeezone/constant.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/model.dart';




class AddressListView extends StatelessWidget {

  final List<Address> addresses;

  final bool isLoading;

  AddressListView(this.isLoading, this.addresses);

  @override
  Widget build(BuildContext context) {

    if(this.isLoading) {
      return new Center(child: new CircularProgressIndicator());
    }

    return addresses.isEmpty?Text(''): Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
      child: Card(
        color: kmaincolor,
        
        child: Padding(
          padding: const EdgeInsets.only(left: 20,top: 5),
          child: Text(' ${this.addresses[0].featureName},${this.addresses[1].featureName},${this.addresses[2].featureName},${this.addresses[3].featureName}'),
        )),
    );
    
  }
}

