import 'package:coffeezone/constant.dart';
import 'package:coffeezone/provider/order.dart';
import 'package:coffeezone/provider/theme.dart';
import 'package:coffeezone/screens/widgets.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import '../widgets/cartorder.dart';

import 'package:provider/provider.dart';
import '../provider/cart.dart';

class MyCart extends StatefulWidget {
  @override
  _MyCartState createState() => _MyCartState();
}

class _MyCartState extends State<MyCart> {
   
  List<Address> results = [];

  bool isLoading = false;
   Future search() async {

final loc=Provider.of<Order>(context,listen: false);
    this.setState(() {
      this.isLoading = true;
    });

    try{
      var geocoding = Geocoder.local;
      
      var results = await geocoding.findAddressesFromCoordinates( Coordinates(loc.getPickedLocation[0], loc.getPickedLocation[1]));
    
      this.setState(() {
        this.results = results;
      });
    }
    catch(e) {
      print("Error occured: $e");
    }
    finally {
      this.setState(() {
        this.isLoading = false;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    
    final data = Provider.of<Cart>(context).items;
    final theme=Provider.of<ThemeChanger>(context);
   final order=Provider.of<Order>(context,listen: false);
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.only(left: 20),
          child:Icon(Icons.shopping_cart,color:theme.getTheme?kmaincolor: kseccolor),
        ),
        automaticallyImplyLeading: false,
        backgroundColor:theme.getTheme?Colors.grey[850]: Colors.white,
        elevation: 0,
        title: Text('My Cart',style: TextStyle(color:theme.getTheme?kmaincolor: kseccolor),),
      ),
      backgroundColor:theme.getTheme?Colors.grey[850]: Colors.white70,
      body: ListView(
        children:<Widget> [
          
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height*.6,
            child: ListView.builder(
              shrinkWrap: true,
              
              itemBuilder: (ctx, i) => CartOrder(
                  data.values.toList()[i].amount,
                  data.values.toList()[i].id,
                  data.values.toList()[i].imgurl,
                  data.values.toList()[i].price,
                  data.keys.toList()[i],
                  data.values.toList()[i].title),
              itemCount: data.length,
              ),
          ),
      Padding(
        padding: const EdgeInsets.only(bottom: 0),
        child:  Container(
        
            child:  Center(
            child: Column(
              
              children: <Widget>[
                results.isEmpty?Text(''): Expanded(child: new AddressListView(this.isLoading, this.results)),
                Padding(
                  padding: const EdgeInsets.only(left: 120,right: 120,bottom: 30),
                  child:results.isEmpty&& data.isNotEmpty? RaisedButton(
                  color: kmaincolor,
                    onPressed: () async {
                      LocationResult result = await showLocationPicker(
                        context,
                      ' AIzaSyB1GJqFpDlAUaTEBQKaTCuDTbjyPOKkwrA'
                       
                       
                     
                      );
                    setState(() {
                      
                      Provider.of<Order>(context,listen: false).setPickedLocation(result);
                    
                      search();
                     
                    });
                     
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                      Icon(Icons.map),
                      Text(' Pick location')
                      ],
                    
                    )
                  ):Container()
                )
                ,
            results.isEmpty?Container():Padding(
              padding: const EdgeInsets.only(left: 30),
              child: Row(
                children: [
                  data.isNotEmpty?RaisedButton(color: kmaincolor,onPressed: (){},child: Text('${Provider.of<Orders>(context,listen:false).total(data.values.toList())} L.E',style: TextStyle(fontWeight: FontWeight.bold),)):Text(''),
                 Spacer(),
                  RaisedButton(onPressed: (){
Provider.of<Orders>(context,listen: false).addOrder(Order(orders:data.values.toList(), pickedLocation:order.pickedLocation, id: DateTime.now().toString()));
                  },child:Text('Order Now',),color: kmaincolor,),
                  SizedBox(width: MediaQuery.of(context).size.width*.08,)
                ],
              ),
            )
                  ,
              ],)),
            height: MediaQuery.of(context).size.height*.2,

          )
      )
        ],
      )
    );
  }
}
