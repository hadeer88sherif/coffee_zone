
import 'package:flutter/material.dart';
import '../constant.dart';
import '../provider/cart.dart';




import '../widgets/buildcontainer.dart';
import 'package:provider/provider.dart';

class OrderScreen extends StatefulWidget {
  static const routname = 'order';
  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  
  var val1= false;
 var val2= false;
  var val3= false;
    var cube0 = false;
     var cube1 = false;
  var cube2= false;
   var cube3 = false;
  var milk =false;
  var creama=false;
  
  @override
  Widget build(BuildContext context) {
    final id = ModalRoute.of(context).settings.arguments as String;
 

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    final carti=Provider.of<Cart>(context);
final cart=Provider.of<Cart>(context).cart(id);
var amount=cart.amount;
  
    return Scaffold(
      body: ListView(children: <Widget>[
        Stack(
          children: <Widget>[
            Container(
              child: Image.asset(
                'assets/bg2.jpg',
                fit: BoxFit.cover,
              ),
              height: height * .3,
              width: width,
            ),
            Container(
              color: kmaincolor.withOpacity(.8),
              width: width,
              height: height * .3,
              child: Center(
                child: FittedBox(child: Image.asset(cart.imgurl)),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(left: 30, top: 20, bottom: 10),
          child: Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text(
                    cart.title,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Colors.brown,
                        fontSize: 20),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text(
                      '${cart.price} EGP',
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
              Spacer(),
              Builder(
                builder: (context) => Container(
                  margin: EdgeInsets.only(right: 5),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.brown),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10))),
                  width: width * .08,
                  height: height * .025,
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        if (amount > 0) {
                          carti.remove(id);
                          
                        } else {
                          Scaffold.of(context).hideCurrentSnackBar();
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text('No Amount Added'),
                            duration: Duration(seconds: 2),
                          ));
                        }
                      });
                    },
                    child: Icon(
                      Icons.remove,
                      size: 16,
                    ),
                  ),
                ),
              ),
              Text('$amount'),
              Container(
                margin: EdgeInsets.only(left: 5, right: 15),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.brown),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        bottomRight: Radius.circular(10))),
                width: width * .08,
                height: height * .025,
                child: InkWell(
                  onTap: () {
                    setState(() {
                     
                      carti.addInCart(proid: id,title: cart.title,price: cart.price,imgurl: cart.imgurl,);
                    });
                  },
                  child: Icon(
                    Icons.add,
                    size: 16,
                  ),
                ),
              )
            ],
          ),
        ),
        Divider(),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
          child: Row(
            children: <Widget>[
              Text(
                'Size',
                style: TextStyle(fontSize: 16, color: Colors.brown),
              ),
              Spacer(),
              InkWell(
                onTap: () {
                  setState(() {
                    val1 =true;
                    val2=false;
                    val3=false;
                   
                  });
                },
                child: BuildContainers(
                    f: val1,
                    size: width * .07,
                    imgurl: 'assets/non.png',
                    top: height * .035,
                    left: width * .03),
              ),
              SizedBox(
                width: width * .03,
              ),
              InkWell(onTap: (){
                setState(() {
                    val1 =false;
                    val2=true;
                    val3=false;
                   
                  });
              },
                              child: BuildContainers(
                    f: val2,
                    imgurl: 'assets/non.png',
                    size: width * .09,
                    top: height * .03,
                    left: width * .02),
              ),
              SizedBox(
                width: width * .03,
              ),
              InkWell(
                onTap: (){
                  setState(() {
                    val1 =false;
                    val2=false;
                    val3=true;
                   
                  });
                },
                              child: BuildContainers(
                    f: val3,
                    imgurl: 'assets/non100.png',
                    size: width * .12,
                    top: height * .02,
                    left: width * .01),
              ),
              SizedBox(
                width: width * .19,
              )
            ],
          ),
        ),
        Divider(),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
          child: Row(
            children: <Widget>[
              Text(
                'Sugar',
                style: TextStyle(fontSize: 16, color: Colors.brown),
              ),
              Spacer(),
              InkWell(
                onTap: (){
                  setState(() {
                    cube0=true;
                  cube1=false;
                  cube2=false;
                  cube3=false;
                  });
                },
                              child: BuildContainers(
                  f:cube0,
                  size: width * .1,
                  imgurl: 'assets/3free.png',
                  top: height * .02,
                  left: width * .02,
                ),
              ),
              SizedBox(
                width: width * .03,
              ),
              InkWell(
                onTap: (){
                  setState(() {
                    cube0=false;
                  cube1=true;
                  cube2=false;
                  cube3=false;
                  });
                },
                              child: BuildContainers(
                    f:cube1,
                    imgurl: 'assets/onecube.png',
                    size: width * .1,
                    top: height * .02,
                    left: width * .02),
              ),
              SizedBox(
                width: width * .03,
              ),
              InkWell(
                onTap: (){
                 setState(() {
                    cube2=true;
                  cube1=false;
                  cube0=false;
                  cube3=false;
                 });
                },
                              child: BuildContainers(
                    f:cube2,
                    imgurl: 'assets/2cube.png',
                    size: width * .12,
                    top: height * .03,
                    left: width * .0),
              ),
              SizedBox(
                width: width * .03,
              ),
              InkWell(
                onTap: (){
                  setState(() {
                    cube3=true;
                  cube1=false;
                  cube2=false;
                  cube0=false;
                  });
                },
                              child: BuildContainers(
                    f:cube3,
                    imgurl: 'assets/3cube.png',
                    size: width * .12,
                    top: height * .015,
                    left: width * .01),
              ),
              SizedBox(
                width: width * .035,
              )
            ],
          ),
        ),
        Divider(),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
          child: Row(
            children: <Widget>[
              Text(
                'Addition',
                style: TextStyle(fontSize: 16, color: Colors.brown),
              ),
              Spacer(),
              InkWell(
                onTap: (){
                  setState(() {
                    milk=!milk;
                  });
                },
                              child: BuildContainers(
                    f: milk,
                    size: width * .1,
                    imgurl: 'assets/milk.png',
                    top: height * .02,
                    left: width * .02),
              ),
              SizedBox(
                width: width * .03,
              ),
              InkWell(
                onTap: (){
                  setState(() {
                    creama=!creama;
                  });
                },
                              child: BuildContainers(
                    f: creama,
                    imgurl: 'assets/cream.png',
                    size: width * .09,
                    top: height * .03,
                    left: width * .02),
              ),
              SizedBox(
                width: width * .35,
              )
            ],
          ),
        ),
        Builder(
          builder: (context)=>
                 InkWell(
            onTap: () {
           Scaffold.of(context).showSnackBar(SnackBar(content:Text('Order Added'),duration: Duration(seconds: 2),));
              
            Provider.of<Cart>(context,listen: false).addInCart(proid: id,additions:additions(milk, creama),size:size(val1, val2),sugar: sugar(cube0, cube1, cube2)  );
print(Provider.of<Cart>(context,listen: false).items['5'].additions);
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 30, left: 280, right: 30),
              child: Container(
                width: width * .1,
                height: height * .05,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5), color: Colors.brown),
                child: Center(
                  child: Text(
                    'Conferm',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        color: Colors.brown.shade100),
                  ),
                ),
              ),
            ),
          ),
        )
      ]),
    );
  }
}
