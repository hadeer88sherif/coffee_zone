import 'package:coffeezone/screens/edit.dart';

import 'package:coffeezone/widgets/admin_widget.dart';
import 'package:flutter/material.dart';
import '../constant.dart';

import 'package:provider/provider.dart';
import '../provider/coffee.dart';

enum FilterOptions { Donuts, Coffee }

class AdminPanel extends StatefulWidget {
  static const routname = 'home';

  @override
  _AdminPanelState createState() => _AdminPanelState();
}

class _AdminPanelState extends State<AdminPanel> {
  
   var _coffeePro=false;
  @override
  Widget build(BuildContext context) {
   final prodata = Provider.of<Products>(context).items;
   final prodDonut=Provider.of<Products>(context).dounts;
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
         
          IconButton(
              icon: Icon(
                Icons.add_shopping_cart,
                color: kseccolor,
              ),
              onPressed: () {
                Navigator.pushNamed(context, EditScreen.routname);
              }),
               PopupMenuButton(
            onSelected: (FilterOptions val){
              if(val==FilterOptions.Coffee){
                setState(() {
                  _coffeePro=true;
                });
              }
              else{
                setState(() {
                  _coffeePro=false;
                });
              }
            },
            icon: Icon(Icons.more_vert,color: kseccolor,),
              itemBuilder: (_) => [
                    PopupMenuItem(
                      child: Text('Donuts',style: TextStyle(color: kseccolor),),
                      value: FilterOptions.Donuts,
                    ),
                    PopupMenuItem(child:Text('Coffee',style: TextStyle(color: kseccolor)),value: FilterOptions.Coffee,)
                  ]),
          SizedBox(
            width: MediaQuery.of(context).size.width * .05,
          ),
        ],
        leading: Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Image.asset('assets/logo.png'),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Admin Panel',
          style: TextStyle(color: kseccolor, fontSize: 17),
        ),
        automaticallyImplyLeading: false,
      ),
      body:Padding(
        padding: const EdgeInsets.only(top: 20),
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, childAspectRatio: 4 / 5),
          itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
              value:_coffeePro? Product(
                  type: prodata[i].type,
                  id: prodata[i].id,
                  imgurl: prodata[i].imgurl,
                  title: prodata[i].title,
                  price: prodata[i].price):Product(
                  type: prodDonut[i].type,
                  id: prodDonut[i].id,
                  imgurl: prodDonut[i].imgurl,
                  title: prodDonut[i].title,
                  price: prodDonut[i].price),
              child: AdminProduct()),
          itemCount:_coffeePro? prodata.length:prodDonut.length,
        ),
      )
    );
  }
}
