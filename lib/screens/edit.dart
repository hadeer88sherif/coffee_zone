import 'dart:io';

import 'package:coffeezone/constant.dart';
import 'package:coffeezone/provider/coffee.dart';
import 'package:coffeezone/provider/theme.dart';
import 'package:coffeezone/widgets/textform.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';

 enum ProductType { Coffee, Donut }

class EditScreen extends StatefulWidget {
  static const routname = 'edit';
  
  @override
  _EditScreenState createState() => _EditScreenState();
}

class _EditScreenState extends State<EditScreen> {
  final _form = GlobalKey<FormState>();
  final _priceFocusNode = FocusNode();

  final _img = FocusNode();
  var _editedProduct =
      Product(id: null, price: 0.0, type: null, imgurl: '', title: '');
  var _initalValue = {'title': '', 'type': '', 'price': '', 'img': ''};
  var _init = true;
  @override
  void didChangeDependencies() {
    if (_init) {
      final id = ModalRoute.of(context).settings.arguments as String;

      if (id != null) {
        _editedProduct = Provider.of<Products>(context).findById(id);

        _initalValue = {
          'title': _editedProduct.title,
          'type': _editedProduct.type.toString(),
          'price': _editedProduct.price.toString(),
          'img': _editedProduct.imgurl
        };
      }
    }
    _init = false;

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _priceFocusNode.dispose();

    _img.dispose();
    super.dispose();
  }

  void _saveForm() {
    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }
    _form.currentState.save();
    if (_editedProduct.id != null) {
      Provider.of<Products>(context, listen: false)
          .updateProduct(_editedProduct.id, _editedProduct);
      print(_editedProduct.type);
    } else {
      Provider.of<Products>(context, listen: false).addproduct(_editedProduct);
    }
    Navigator.of(context).pop();
  }
  bool isImageLoaded=false;
   File pickedImage;
    void pickImage() async {
    var tempStore = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
       _editedProduct = Product(
                        id: _editedProduct.id,
                        imgurl: _editedProduct.imgurl,
                        price: _editedProduct.price,
                        title: _editedProduct.title,
                        type: _editedProduct.type);
      pickedImage = tempStore;
      isImageLoaded=true;
    });
  }

ProductType _proType ;
  @override
  Widget build(BuildContext context) {
    final theme=Provider.of<ThemeChanger>(context);
    _proType=_editedProduct.type;
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Image.asset('assets/logo.png'),
        ),
        backgroundColor:theme.getTheme?Colors.grey[850]: Colors.white70,
        elevation: 0,
        title: Text('Edit ', style: TextStyle(color:theme.getTheme?kmaincolor: kseccolor)),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
              color:theme.getTheme?kmaincolor: kseccolor,
            ),
            onPressed: () {
              _saveForm();
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Form(
            key: _form,
            child: ListView(
              children: <Widget>[
                Textfield(
                  initalVal: _initalValue['title'],
                  hint: 'Title',
                  action: TextInputAction.next,
                  node: _priceFocusNode,
                  onsaved: (val) {
                    _editedProduct = Product(
                        id: _editedProduct.id,
                        imgurl: _editedProduct.imgurl,
                        price: _editedProduct.price,
                        title: val,
                        type: _editedProduct.type);
                  },
                ),
                ListTile(
                  leading: Radio(
                      value: ProductType.Coffee,
                      groupValue: _proType,
                      activeColor:theme.getTheme?kmaincolor: kseccolor,
                      onChanged: (ProductType val) {
                        setState(() {
                          _proType = val;
                          _editedProduct = Product(
                              id: _editedProduct.id,
                              imgurl: _editedProduct.imgurl,
                              price: _editedProduct.price,
                              title: _editedProduct.title,
                              type: val);
                          
                        });
                      }),
                  title: Text('Coffee'),
                ),
                ListTile(
                  leading: Radio(
                    activeColor:theme.getTheme?kmaincolor:kseccolor,
                      value: ProductType.Donut,
                      groupValue: _proType,
                      onChanged: (ProductType val) {
                        setState(() {
                           _proType = val;
                          _editedProduct = Product(
                              id: _editedProduct.id,
                              imgurl: _editedProduct.imgurl,
                              price: _editedProduct.price,
                              title: _editedProduct.title,
                              type: val);
                         
                        });
                      }),
                  title: Text('Donut'),
                ),
                Textfield(
                  initalVal: _initalValue['price'],
                  onsaved: (val) {
                    _editedProduct = Product(
                        id: _editedProduct.id,
                        imgurl: _editedProduct.imgurl,
                        price: double.parse(val),
                        title: _editedProduct.title,
                        type: _editedProduct.type);
                  },
                  hint: 'price',
                  action: TextInputAction.next,
                  node: _img,
                  nodes: _priceFocusNode,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15,right: 50,left: 10),
                  child: Row(
                    children: <Widget>[
                     Container(
                       child:isImageLoaded? FittedBox(child: Image.file(pickedImage),fit:BoxFit.fill ,):
                      Text('Select Product ',style: TextStyle(color:kmaincolor),),
                       decoration: BoxDecoration(
                         
                         color: kseccolor,
                         borderRadius: BorderRadius.circular(10)
                       ),
                        width: MediaQuery.of(context).size.width*.3,
                        height: MediaQuery.of(context).size.height*.12,
                        
                      ), 
                        SizedBox(width: MediaQuery.of(context).size.width*.1 ,),
                      FloatingActionButton(onPressed: (){
                        pickImage();


                      },child: Icon(Icons.image,color:kmaincolor,),backgroundColor:kseccolor ,),
                  
                    
                    ],
                  ),
                )
               
              ],
            )),
      ),
    );
  }
}
