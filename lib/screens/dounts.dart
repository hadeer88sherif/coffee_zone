import 'package:coffeezone/provider/theme.dart';
import 'package:coffeezone/screens/edit.dart';
import 'package:flutter/material.dart';
import '../constant.dart';

import '../widgets/productitem.dart';
import 'package:provider/provider.dart';
import '../provider/coffee.dart';

class Dounts extends StatelessWidget {
  static const routname = 'home';
  @override
  Widget build(BuildContext context) {
    final prodata = Provider.of<Products>(context).dounts;
final theme=Provider.of<ThemeChanger>(context);
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Image.asset('assets/logo.png'),
        ),
        backgroundColor:theme.getTheme?Colors.grey[850]: Colors.white70,
        elevation: 0,
        title: Text(
          'More About Dounts',
          style: TextStyle(color:theme.getTheme?kmaincolor: kseccolor, fontSize:17),
        ),
        automaticallyImplyLeading: false,
      ),
      backgroundColor:theme.getTheme?Colors.grey[850]: Colors.white70,
      body: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, childAspectRatio: 4 / 5),
          itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
              value: Product(
                type: ProductType.Donut,
                  id: prodata[i].id,
                  imgurl: prodata[i].imgurl,
                  title: prodata[i].title,
                  price: prodata[i].price),
              child: ProductItem()),
          itemCount: prodata.length,
        ),
      ),
    );
  }
}
