import 'package:coffeezone/constant.dart';
import 'package:coffeezone/provider/theme.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class SettingScreen extends StatefulWidget {
 
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  

  @override
  
  Widget build(BuildContext context) {
    final theme=Provider.of<ThemeChanger>(context);
    return Scaffold(
      appBar: AppBar(
        
        leading:  Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Icon(Icons.settings,color:theme.getTheme? kmaincolor:kseccolor,),
        ),
        elevation: 0,
        backgroundColor:theme.getTheme?Colors.grey[850] :Colors.white70,
        title: Text('Setting',style: TextStyle(color:theme.getTheme? kmaincolor:kseccolor),),
      ),
      backgroundColor:theme.getTheme?Colors.grey[850]: Colors.white70,
      body: Column(
        children: [
          ListTile(

            title:theme.getTheme? Text('Dark Mode',style: TextStyle(color: kmaincolor),): Text('Light Mode'),
            leading:theme.getTheme? Icon(Icons.mood_bad,color: kmaincolor,):Icon(Icons.mood),
            trailing: Switch(
              activeColor: kmaincolor,
              value: theme.getTheme, onChanged: (newval){
              setState(() {
              
                theme.setter(newval);
              });
            }),
          ),
          Divider()
        ],
      ),
    );
  }
}