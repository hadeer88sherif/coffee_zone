import 'package:coffeezone/provider/cart.dart';
import 'package:coffeezone/provider/coffee.dart';
import 'package:coffeezone/provider/order.dart';
import 'package:coffeezone/provider/theme.dart';
import 'package:coffeezone/screens/edit.dart';
import 'package:coffeezone/screens/homepage.dart';

import 'package:coffeezone/screens/orderscreen.dart';
import 'package:coffeezone/widgets/bottom_navigator.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main()=>runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
          
        ChangeNotifierProvider.value(
      
      value: Cart(),),
      ChangeNotifierProvider.value(
      
      value: Order(id:null,orders:null,pickedLocation: null  ),),
       ChangeNotifierProvider.value(value:Orders()),
      ChangeNotifierProvider.value(value:ThemeChanger()),
      ChangeNotifierProvider.value(
      
      value:Products(),)
      ],


          child: MaterialAppWidget(),
    );
  }
}

class MaterialAppWidget extends StatelessWidget {
 

  @override
  Widget build(BuildContext context) {
  
    return MaterialApp(
    
  
        debugShowCheckedModeBanner: false,
        home:BottomNavBar(),
        
       
        routes: {
    EditScreen.routname:(ctx)=>EditScreen(),
     OrderScreen.routname:(ctx)=>OrderScreen(),
    HomePage.routname:(ctx)=>HomePage(),
    BottomNavBar.routname:(ctx)=>BottomNavBar(),
        
         
        }
      );
  }
}












  
