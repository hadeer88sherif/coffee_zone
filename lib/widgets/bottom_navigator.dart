import 'package:coffeezone/provider/theme.dart';
import 'package:coffeezone/screens/setting.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:provider/provider.dart';
import '../constant.dart';
import '../screens/dounts.dart';
import '../screens/homepage.dart';

import '../screens/mycart.dart';



class BottomNavBar extends StatefulWidget {
  static const routname='main';
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
   int pageIndex = 0;
  GlobalKey _bottomNavigationKey = GlobalKey();
  var pages=[
    HomePage(),
    
    Dounts(),
    MyCart(),
   
    SettingScreen()

  ];
  @override
  Widget build(BuildContext context) {
    final theme=Provider.of<ThemeChanger>(context);
    return Scaffold(
      bottomNavigationBar:CurvedNavigationBar(
        key: _bottomNavigationKey,
        index: pageIndex,
        height: 50,
          color:theme.getTheme?Colors.grey[850]: Colors.white,
          buttonBackgroundColor:theme.getTheme?Colors.grey[850]: Colors.white,
          backgroundColor:kmaincolor,
          animationCurve: Curves.easeInOut,
          animationDuration: Duration(milliseconds: 600),
          onTap: (int index){
            setState(() {
               pageIndex=index;
            });
           
          },
        items: <Widget>[
 
  Image.asset('assets/tea.png',width: MediaQuery.of(context).size.width*.2,height: MediaQuery.of(context).size.height*.05 ),
        Image.asset('assets/donut.png',width: MediaQuery.of(context).size.width*.2,height: MediaQuery.of(context).size.height*.05 ,),
        Icon(Icons.shopping_cart,size: 30,color: theme.getTheme?kmaincolor:Colors.brown),
       
        Icon(Icons.settings,size: 30,color: theme.getTheme?kmaincolor:Colors.brown)
      ],),
      body: pages[pageIndex],
    
    );
  }
}