import 'package:flutter/material.dart';

class Textfield extends StatelessWidget {
  final String hint;
final TextInputAction action;
final  node;
 final nodes;
final Function onsaved;
final initalVal;
  Textfield({this.initalVal,this.onsaved,this.nodes,this.hint,this.action,this.node});


  @override
  
  Widget build(BuildContext context) {
   
    return Padding(
      padding: const EdgeInsets.only(top:10,bottom: 10,left:10 ,right:20),
      child: TextFormField(
        
        validator: (value){
          if(value.isEmpty){
             return 'Please provide a value.';
          }
          
          return null;
        },
        initialValue: initalVal,
        onSaved: onsaved,
        focusNode: nodes,
        textInputAction:action,
     onFieldSubmitted: (_){
       FocusScope.of(context).requestFocus(node);
     },


          keyboardAppearance: Brightness.dark,
          cursorColor:Color(0xFFa87756),
          decoration: InputDecoration( 
           
          counterText: '',
          
            contentPadding: EdgeInsets.symmetric(vertical: 15,horizontal: 10),
      hintStyle: TextStyle(color: Color(0xFFa87756),fontSize: 13),
      hintText: hint,
        
          
      enabledBorder: OutlineInputBorder(
         
          borderSide: BorderSide(color:Color(0xFFa87756))),
      focusedBorder: OutlineInputBorder(
         
          borderSide: BorderSide(color:Color(0xFFa87756))),
 errorBorder: OutlineInputBorder(
borderSide: BorderSide(color:Colors.red)
 )   
           ),
        
        ),

    );}}

   