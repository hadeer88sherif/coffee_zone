import 'package:coffeezone/screens/edit.dart';
import 'package:flutter/material.dart';
import '../constant.dart';

import '../provider/coffee.dart';

import 'package:provider/provider.dart';

class AdminProduct extends StatefulWidget {
  
  @override
  _AdminProductState createState() => _AdminProductState();
}

class _AdminProductState extends State<AdminProduct> {
  
  
  @override
  Widget build(BuildContext context) {
    final orddata = Provider.of<Product>(context, listen: false);
  


    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
   
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child:    
                   Dismissible(
                     onDismissed: (_){
                     
                       Provider.of<Products>(context,listen: false).deleteProduct(orddata.id);

                     },
                     key: ValueKey(orddata.id),
                     direction: DismissDirection.startToEnd,
                     confirmDismiss: (direction){
                       return showDialog(context:context,
                       builder: (ctx)=>AlertDialog(
                         title:Text('Are You Sure ?') ,
                         content: Text('are you sure you want to delete this item ?'),
                         actions: <Widget>[
                           FlatButton(onPressed: (){Navigator.of(ctx).pop(false);}, child:Text('No')),
                           FlatButton(onPressed: (){Navigator.of(ctx).pop(true);}, child: Text('Yes'))
                         ],
                       ),
                       );
                     },
              child: Stack(
          children: <Widget>[
            Container(
              height: height,
              width: width,
            ),
            Positioned(
              top: height * .05,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: kmaincolor,
                ),
                height: height * .17,
                width: width * .35,
              ),
            ),
            Positioned(
                top: height * .24,
                child: Container(
                  width: width * .35,
                  height: height * .032,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.orange.shade900),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 7),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        
                     InkWell(
                       onTap: (){
                         showDialog(context:context,
                       builder: (ctx)=>AlertDialog(
                         title:Text('Are You Sure ?') ,
                         content: Text('are you sure you want to delete this item ?'),
                         actions: <Widget>[
                           FlatButton(onPressed: (){Navigator.of(ctx).pop(false);}, child:Text('No')),
                           FlatButton(onPressed: (){ 
                             Provider.of<Products>(context,listen: false).deleteProduct(orddata.id);
                             Navigator.of(ctx).pop(true);
                             }, child: Text('Yes'))
                         ],
                       ),
                       );
                         
                       },
                                            child: Icon(
                         Icons.delete_sweep,
                         color: Colors.white54,
                       ),
                     ),
                        InkWell(
                          onTap: (){
                            
                           Navigator.push(context, MaterialPageRoute(builder: (ctx)=>EditScreen()));
                           
                          },
                                              child: Text(
                            'Edit',
                            style: TextStyle(
                                color: Colors.white70, fontWeight: FontWeight.w500),
                          ),
                        ),
                        InkWell(
                            onTap: () {
                         Navigator.pushNamed(context,EditScreen.routname,arguments: orddata.id );
                       
             
                            },
                            child: Icon(
                              Icons.edit,
                              color: Colors.white54,
                            )),
                      ],
                    ),
                  ),
                )),
            Positioned(
              bottom: height * .17,
              left: width * .07,
              child: Column(
                children: <Widget>[
                  Container(
                      width: width * .2,
                      height: height * .1,
                      child: Image.asset(orddata.imgurl,fit: BoxFit.fill,)),
                  Text(
                    orddata.title,
                    style: TextStyle(color: kseccolor,fontWeight: FontWeight.w600),

                  )
                  ,
                ],
              ),

            ),
            Positioned(
               bottom: height * .13,
              left: width*.03,
                        child: Column(
                children: <Widget>[
                  Text('${orddata.price} LE',style: TextStyle(color:kseccolor,fontWeight: FontWeight.w400),)
                ],
              ),
            ),
           
  
          ]
        ),
      ),
    );
    
  }
  
}


