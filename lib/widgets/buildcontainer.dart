import 'package:flutter/material.dart';
import '../constant.dart';

class BuildContainers extends StatefulWidget {
  const BuildContainers(
      {@required this.imgurl,
      @required this.size,
      @required this.top,
      @required this.f,
    
      
      
      @required this.left});
  final double left;
  final double top;
  final String imgurl;
  final double size;
  final bool f;
  
  

  @override
  _BuildContainersState createState() => _BuildContainersState();
}

class _BuildContainersState extends State<BuildContainers> {
  
  
  @override
  Widget build(BuildContext context) {
   
    return Stack(
        children: <Widget>[
    Positioned(
      top: widget.top,
      left: widget.left,
      child: Container(
          width: widget.size,
          child: Image.asset(widget.imgurl),
        ),
    ),
        Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color:widget.f? kmaincolor.withOpacity(.1):kmaincolor.withOpacity(.8),
            ),
            width: MediaQuery.of(context).size.width * .13,
            height: MediaQuery.of(context).size.height * .08,
      )
        ],
      );
  }
}
