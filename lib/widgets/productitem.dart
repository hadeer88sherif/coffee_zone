import 'package:flutter/material.dart';
import '../constant.dart';
import '../provider/cart.dart';
import '../provider/coffee.dart';
import '../screens/mycart.dart';
import 'package:provider/provider.dart';

class ProductItem extends StatefulWidget {
  
  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  var val=0;
  var f = false;
  @override
  Widget build(BuildContext context) {
    final orddata = Provider.of<Product>(context, listen: false);
    final cart=Provider.of<Cart>(context,listen: false);


    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
   
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Stack(
        children: <Widget>[
          Container(
            height: height,
            width: width,
          ),
          Positioned(
            top: height * .05,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: kmaincolor,
              ),
              height: height * .17,
              width: width * .35,
            ),
          ),
          Positioned(
              top: height * .24,
              child: Container(
                width: width * .35,
                height: height * .03,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.orange.shade900),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    
                val>0?    InkWell(
                        onTap: () {
                          setState(() {
                            val-=1;
                            
                          });
                          cart.remove(orddata.id);
                        },
                        child: Icon(
                          Icons.remove,
                          color: Colors.white54,
                        )):Container(),
                    InkWell(
                      onTap: (){
                        
                        Navigator.of(context).push(MaterialPageRoute(builder: (ctx)=>MyCart()));
                       
                      },
                                          child: Text(
                        'Add',
                        style: TextStyle(
                            color: Colors.white70, fontWeight: FontWeight.w500),
                      ),
                    ),
                    InkWell(
                        onTap: () {
                     setState(() {
                       f=true;
                       val++;
                       
                     });   
                    cart.addInCart( proid: orddata.id,title: orddata.title,price: orddata.price,imgurl: orddata.imgurl);
           
                        },
                        child: Icon(
                          Icons.add,
                          color: Colors.white54,
                        )),
                  ],
                ),
              )),
          Positioned(
            bottom: height * .17,
            left: width * .07,
            child: Column(
              children: <Widget>[
                Container(
                    width: width * .2,
                    height: height * .1,
                    child: Image.asset(orddata.imgurl,fit: BoxFit.fill,)),
                Text(
                  orddata.title,
                  style: TextStyle(color: kseccolor,fontWeight: FontWeight.w600),

                )
                ,
              ],
            ),

          ),
          Positioned(
             bottom: height * .13,
            left: width*.03,
                      child: Column(
              children: <Widget>[
                Text('${orddata.price} LE',style: TextStyle(color:kseccolor,fontWeight: FontWeight.w400),)
              ],
            ),
          ),
         
   f&(val>0)? Positioned(
            top: height*.04,
            
            child: Container(
            
            width: width*.049,
            height: width*.049,
            decoration: BoxDecoration(
              color: Colors.orange.shade900,
              borderRadius: BorderRadius.circular(30)
            ),
            child: Center(child: Text('$val',style: TextStyle(color: Colors.white),)),
          ) ):Container()
        ]
      ),
    );
    
  }
  
}


