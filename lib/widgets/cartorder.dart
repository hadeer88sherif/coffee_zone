import 'package:coffeezone/provider/cart.dart';
import 'package:coffeezone/provider/coffee.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../constant.dart';
import '../screens/orderscreen.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
class CartOrder extends StatelessWidget {
  final String id;
  final String title;
  final String imgurl;
  final double price;
  final String prodid;
  final int amount;
  CartOrder(
      this.amount, this.id, this.imgurl, this.price, this.prodid, this.title);
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    final cartid=Provider.of<Cart>(context,listen: false).proid(id);
    
    return Stack(
      children: <Widget>[
        Container(
          width: width,
          height: height * .15,
        ),
        Positioned(
          top: height * .02,
          left: width * .75,
          child: Container(
              child: Image.asset(
                'assets/cloud1.png',
                fit: BoxFit.fill,
              ),
              width: width * .25,
              height: height * .15),
        ),
        Positioned(
          top: height * .02,
          left: width * .75,
          child: Container(
              child: Image.asset(
                'assets/cloud.png',
                fit: BoxFit.fill,
              ),
              width: width * .2,
              height: height * .1),
        ),
        Positioned(
          top: height * .09,
          left: width * .8,
          child: Text(
            '$price X $amount',
            style: TextStyle(
                color: Colors.black38,
                fontWeight: FontWeight.w500,
                fontSize: 16),
          ),
        ),
        Positioned(
            top: height * .03,
            left: width * .03,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: kmaincolor,
              ),
              width: width * .7,
              height: height * .11,
            )),
        Positioned(
            top: height * .05,
            left: width * .35,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Review This Product',
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.brown,
                    )),
                    SizedBox(height: height*.01,),

                    Padding(
                      padding: const EdgeInsets.only(right: 45),
                      child: RatingBar(
                        unratedColor: Colors.grey[400],
                        itemSize: 25,
                        glowColor: kseccolor,
   initialRating: 0,
   minRating: 1,
   direction: Axis.horizontal,
   allowHalfRating: true,
   itemCount: 5,
   
   itemBuilder: (context, _) => Icon(
     Icons.star,
     color: Colors.amber,
   ),
   onRatingUpdate: (rating) {

   Provider.of<Cart>(context,listen: false).addInCart(proid: cartid,rating: rating);
   print('${ Provider.of<Cart>(context,listen: false).items[cartid].rating}');
    
   },
),
                    )
                
              ],
            )),
        Positioned(
            top: height * .03,
            left: width * .03,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: kseccolor,
              ),
              width: width * .3,
              height: height * .11,
              child: Padding(
                padding: const EdgeInsets.only(top: 15, left: 30),
                child: Column(
                  children: <Widget>[
                    Text(
                      title,
                      style: TextStyle(
                          fontWeight: FontWeight.w500, color: Colors.brown),
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.of(context).pushNamed(OrderScreen.routname,arguments: prodid);
                      },
                                          child: Container(
                        width: width * .2,
                        height: height * .04,
                        decoration: BoxDecoration(
                            color: Colors.brown.shade900,
                            borderRadius: BorderRadius.circular(10)),
                        child: Center(
                            child: Text(
                          'Customize it',
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        )),
                      ),
                    )
                  ],
                ),
              ),
            )),
        Positioned(
            left: width * .01,
            bottom: height * .03,
            child: Container(
              width: width * .1,
              height: height * .12,
              child: Image.asset(
                imgurl,
                fit: BoxFit.fill,
              ),
            ))
      ],
    );
  }
}
