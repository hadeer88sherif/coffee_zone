

import 'package:coffeezone/provider/cart.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';

class Order with ChangeNotifier{
  final String id;
  LocationResult pickedLocation;
  final List<CartItem> orders;
  Order({@required this.orders, @required this.pickedLocation,@required this.id});
   get getPickedLocation{
    
     List<double>loc=[pickedLocation.latLng.latitude,pickedLocation.latLng.longitude];
    return loc;
  }
  void setPickedLocation(LocationResult newLoc){
    pickedLocation=newLoc;
   
notifyListeners();
  }
}

class Orders with ChangeNotifier{
List<Order> _orders=[];

List<Order> get orders{
  return [..._orders];
}

void addOrder(Order newOrd){
  if(newOrd.id==null){
return;
  }
_orders.add(newOrd);

notifyListeners();
}

String total(List<CartItem> newOrd){

  var len=newOrd.length;
  var sum=0.0;
for(var i=0;i<len;i++){
sum+=double.parse('${newOrd[i].amount}')*newOrd[i].price;
}
return sum.toStringAsFixed(2);
}

}