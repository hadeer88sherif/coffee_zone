

import 'package:coffeezone/screens/edit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Product with ChangeNotifier{
  final String imgurl;
  final String title;
  final String id;
 final double price;
 final ProductType type;
  Product({@required this.type,@required this.price,@required this.id,@required this.imgurl,@required this.title});
}

class Products with ChangeNotifier{
  List<Product>_dounts=[
    Product(imgurl:'assets/donut.png', title: 'Dount',id: '1',price: 19.99,type: ProductType.Donut),
    Product(imgurl:'assets/donutcho.png', title: 'Dount cho',id: '2',price: 36.99,type: ProductType.Donut),
    Product(imgurl:'assets/doughnut.png', title: 'Dount nut',id: '3',price: 88.5,type: ProductType.Donut),
    Product(imgurl:'assets/mini-donut.png', title: 'mini dount',id: '4',price: 99.4,type: ProductType.Donut),
  ];
   List<Product> _items=[
    Product(imgurl:'assets/tea.png', title: 'latte',id: '5',price: 19.99,type: ProductType.Coffee),
    Product(imgurl:'assets/mocha.png', title: 'mocha',id: '6',price: 36.99,type: ProductType.Coffee),
    Product(imgurl:'assets/tea.png', title: 'tea',id: '7',price: 88.5,type: ProductType.Coffee),
    Product(imgurl:'assets/mocha.png', title: 'esperso',id: '8',price: 99.4,type: ProductType.Coffee),
  ];
  List<Product>get items{
    return [..._items];
  }
   List<Product>get dounts{
    return [..._dounts];
  }
  void addproduct(Product pro){
    if(pro.type==ProductType.Coffee){
    _items.add(
      Product(type: pro.type,imgurl: pro.imgurl,title: pro.title,id: DateTime.now().toString(),price: pro.price)
    );}
    else 
    {  
      
      _dounts.add(
     Product(type: pro.type,imgurl: pro.imgurl,title: pro.title,id: DateTime.now().toString(),price:pro.price));
     
   
     }
    notifyListeners();
  }
 
Product findById(String id){
 final prodIndex = _items.indexWhere((prod) => prod.id == id);
     final prodIndex2 = _dounts.indexWhere((prod) => prod.id == id);
    if (prodIndex >= 0) {
     return items[prodIndex] ;
       
    } 
    else if(prodIndex2 >= 0){
     return _dounts[prodIndex2];
      
    }
    else {
    print('...');
    return null;
    }

}


  void updateProduct(String id, Product newProduct) {
    final prodIndex = _items.indexWhere((prod) => prod.id == id);
     final prodIndex2 = _dounts.indexWhere((prod) => prod.id == id);
    if (prodIndex >= 0) {
      _items[prodIndex] = newProduct;
       notifyListeners();
    } 
    else if(prodIndex2 >= 0){
      _dounts[prodIndex2]=newProduct;
      notifyListeners();
    }
    else {
    print('...');
    }
    
  }

  void deleteProduct(String id) {
    _items.removeWhere((prod) => prod.id == id);
    notifyListeners();
  }  
List<Product> selectedValue(bool val){
  if(val ==true){
    return _items;
  }
  else return _dounts;
}
  
}