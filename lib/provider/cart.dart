import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CartItem {
  final String id;
  final double rating;
  final String title;
  final String imgurl;
  final int amount;
  final double price;
  final String size;
  final List<String> additions;
  final String sugar;
  CartItem(
      {@required this.imgurl,
      @required this.rating,
      @required this.amount,
      @required this.size,
      @required this.additions,
      @required this.sugar,
      @required this.id,
      @required this.price,
      @required this.title,
     
      });
}

class Cart with ChangeNotifier {
  Map<String, CartItem> _items = {};
  Map<String, CartItem> get items {
    return {..._items};
  }

  double get totalAmount {
    var total = 0.0;
    _items.forEach((key, cartItem) {
      total = cartItem.price * cartItem.amount;
    });
    return total;
  }
 
int amount(String prodid){
  var total=items[prodid].amount;

notifyListeners();
  return total;
  
}
CartItem cart(String id){
if (_items.containsKey(id))
return _items[id];
else return null;
}
String proid(String id){
return items.keys.firstWhere((value) => items[value].id==id);

}
  void addInCart({double rating,String proid, double price, String title, String imgurl,String sugar,String size,List<String>additions}) {
    if (_items.containsKey(proid)) {
      _items.update(
          proid,
          (existingCartItem) => CartItem(
            additions:additions,
            rating: rating,
            sugar: sugar,
            size:size,
              imgurl: existingCartItem.imgurl,
              amount: existingCartItem.amount + 1,
              id: existingCartItem.id,
              price: existingCartItem.price,
              title: existingCartItem.title)
              
              );

            
    } else {
      _items.putIfAbsent(
          proid,
          () => CartItem(
            rating: rating,
              additions:additions,
            sugar: sugar,
            size:size,
              amount: 1,
              price: price,
              title: title,
              imgurl: imgurl,
              id: DateTime.now().toString()));
              
    }
    
    notifyListeners();
  }

  void remove(String prodid) {
    if(!_items.containsKey(prodid)){return;}
    if (_items[prodid].amount>1) {
      _items.update(
          prodid,
          (existing) => CartItem(
            rating: existing.rating,
            size: existing.size,
            sugar: existing.sugar,
            additions: existing.additions,
              imgurl: existing.imgurl,
              amount: existing.amount - 1,
              id: existing.id,
              price: existing.price,
              title: existing.title));
    }
    else
    _items.remove(prodid);
    
    notifyListeners();
  }

  void clear() {
    _items = {};
    notifyListeners();
  }
}
